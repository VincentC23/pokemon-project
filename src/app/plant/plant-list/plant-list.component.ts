import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Plant } from '../plant';
import { PlantService } from '../plant.service';

@Component({
  selector: 'app-plant-list',
  templateUrl: './plant-list.component.html',
})
export class PlantListComponent implements OnInit {

  plantList: Plant[];

  constructor( 
    private route: Router,
    private plantService: PlantService
  ) {};

  ngOnInit(): void {
    this.plantService.getPlantList().subscribe(plantList => this.plantList = plantList);
  }

  selectPlant(plant: Plant){
      this.route.navigate(['/plant', plant.id]);
  };

}
