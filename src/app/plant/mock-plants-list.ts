import { Plant } from './plant';
  
export const PLANTS: Plant[] = [
    {
        id: 1,
        name: "Kumquat",
        exposure: "Ensoleillée",
        watering: "3 / semaine",
        picture: "https://www.jardiner-malin.fr/wp-content/uploads/2021/01/kumquat.jpg",
        types: ["Fruitier", "Arbuste"],
        created: new Date()
    },
    {
        id: 2,
        name: "Dracaena",
        exposure: "Très lumineuse / Ensoleillée",
        watering: "1 / semaine",
        picture: "https://www.jardiner-malin.fr/wp-content/uploads/2021/10/dracaena.jpg",
        types: ["Plante int"],
        created: new Date()
    },
    {
        id: 3,
        name: "Chamaedorea elegans",
        exposure: "Lumineuse",
        watering: "2 / semaine",
        picture: "https://www.jardiner-malin.fr/wp-content/uploads/2022/12/Arrosage-Chamaedorea-elegans-palmier-nain.jpg",
        types: ["Palmier int"],
        created: new Date()
    },
   
];