import { Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Plant } from '../plant';
import { PlantService } from '../plant.service';


@Component({
  selector: 'app-plant-form',
  templateUrl: './plant-form.component.html',
  styleUrls: ['./plant-form.component.css']
})
export class PlantFormComponent implements OnInit {
  
  @Input() plant: Plant
  
  types: string[];

  constructor(
    private plantService: PlantService, 
    private router: Router, 
    private location: Location){ }

  ngOnInit(): void {
      this.types = this.plantService.getPlantTypeList();
  };
  
  hasType(type: string): boolean {
    return this.plant.types.includes(type);
  };

  selectType($event: Event, type: string){
    const isChecked: boolean = ($event.target as HTMLInputElement).checked;
    
    if(isChecked){
      this.plant.types.push(type);
    }
    else {
      const index: number = this.plant.types.indexOf(type);
      this.plant.types.splice(index, 1);
    }
  };

  onSubmit(){
    console.log ("submit form");
    this.plantService.updatePlant(this.plant)
    .subscribe(() => this.router.navigate(['plant', this.plant.id]))
  }

  isTypesValid(type: string): boolean {
    if(this.plant.types.length == 1 && this.hasType(type)) {
      return false;
    }
    if(this.plant.types.length > 2 && !this.hasType(type)){
      return false;
    }
      return true;
  }

  goBack(){
    this.location.back();
  }
}