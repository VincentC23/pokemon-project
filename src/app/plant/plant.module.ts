import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BorderCardDirective } from './border-card.directive';
import { PlantTypeColorPipe } from './plant-type-color.pipe';
import { PlantListComponent } from './plant-list/plant-list.component';
import { PlantDetailComponent } from './plant-detail/plant-detail.component';
import { RouterModule, Routes } from '@angular/router';
import { PlantService } from './plant.service';
import { FormsModule } from '@angular/forms';
import { PlantFormComponent } from './plant-form/plant-form.component';
import { PlantEditComponent } from './plant-edit/plant-edit.component';

const plantRoutes: Routes = [
  { path: 'plant/edit/:id' , component: PlantEditComponent},
  { path: 'plants' , component: PlantListComponent},
  { path: 'plant/:id' , component: PlantDetailComponent},

];

@NgModule({
  declarations: [
    BorderCardDirective,
    PlantTypeColorPipe,
    PlantListComponent,
    PlantDetailComponent,
    PlantFormComponent,
    PlantEditComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(plantRoutes)
  ],
  providers: [
    PlantService
  ]
})

export class PlantModule { }
