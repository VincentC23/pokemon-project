import { Component, DefaultIterableDiffer, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Plant } from '../plant';
import { PlantService } from '../plant.service';

@Component({
  selector: 'app-plant-detail',
  templateUrl: './plant-detail.component.html',
  styles: [
  ]
})

export class PlantDetailComponent implements OnInit{

  plantList: Plant[];
  plant: Plant|undefined;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private plantService: PlantService
    ) { }

  ngOnInit() {
   this.plantService.getPlantList().subscribe(plantlist => this.plantList = plantlist);
    const plantId: string|null = this.route.snapshot.paramMap.get('id');
    if(plantId){
      this.plantService.getPlantById(+plantId).subscribe(plant => this.plant = plant);
    }
    if(this.plant){
      console.log(this.plant);
    }
  }

  goToPlants(): void {
    this.router.navigate(['/plants']);
  }

  editPlant(plant: Plant): void {
    this.router.navigate(['/plant/edit', plant.id]);
  }

  deletePlant(plant: Plant):void {
    this.plantService.deletePlant(plant.id)
    .subscribe(() => this.goToPlants());
  }

}
