import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'plantTypeColor'
})
export class PlantTypeColorPipe implements PipeTransform {

  transform(type: string): string {
    
    let color: string;
  
    switch (type) {
      case 'Arbuste':
        color = 'brown lighten-3';
        break;
      case 'Fruitier':
        color = 'deep-orange lighten-2';
        break;
      case 'Plante int':
        color = 'green lighten-1';
        break;
      case 'Palmier int':
        color = 'teal lighten-2';
        break;
      default:
        color = 'grey';
        break;
    }
  
    return 'chip ' + color;
  }

}
