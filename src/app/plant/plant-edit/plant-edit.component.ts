import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Plant } from '../plant';
import { PlantService } from '../plant.service';

@Component({
  selector: 'app-plant-edit',
  template: `
    <h2 class="center">Editer {{plant?.name}}</h2>
    <p *ngIf="plant" class="center"> 
      <img style="width: 10%;" [src]="plant.picture" alt="picture">
    </p>
    <app-plant-form *ngIf="plant" [plant]="plant"></app-plant-form>
  `,
  styles: [
  ]
})
export class PlantEditComponent implements OnInit {

  plant: Plant|undefined;

  constructor(private route: ActivatedRoute, private plantService: PlantService){ }

  ngOnInit(): void {
      const plantId: string|null = this.route.snapshot.paramMap.get("id");
      if(plantId){
        this.plantService.getPlantById(+plantId).subscribe(plant => this.plant = plant);
      }
      else{
        this.plant = undefined;
      }
  }
}
