import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap, catchError, of } from 'rxjs';

import { Plant } from './plant';

@Injectable()

export class PlantService {

  constructor( private http: HttpClient){};

  getPlantList(): Observable<Plant[]> {
    return this.http.get<Plant[]>('api/plants').pipe(
      tap((response) => this.log(response)),
      catchError((error) => this.handleError(error, []))
    );
  }

  getPlantById(plantId: number): Observable<Plant|undefined> {
    return this.http.get<Plant>(`api/plants/${plantId}`).pipe(
      tap((response) => this.log(response)),
      catchError((error) => this.handleError(error, undefined))
    );
  }

  updatePlant(plant: Plant): Observable<null>{
    const httpOption =  {
      headers: new HttpHeaders({ 'content-type': 'application/json'})
    };
    return this.http.put('api/plants', plant, httpOption).pipe(
      tap((response) => this.log(response)),
      catchError((error) => this.handleError(error, null))
    )
  }

  deletePlant(plantId: number): Observable<null>{
    return this.http.delete(`api/plants/${plantId}`).pipe(
      tap((response) => this.log(response)),
      catchError((error) => this.handleError(error, null))
    )
  }

  private log(response: any){
    console.table(response);
  }

  private handleError(error: Error, errorValue: any ){
    console.error(error);
    return of(errorValue);
  }

  getPlantTypeList(): string[] {
    return ['Arbuste', 'Fruitier', 'Plante int','Palmier int']
  }

  
}
