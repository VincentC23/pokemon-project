import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appBorderCard]'
})
export class BorderCardDirective {

  constructor(private el: ElementRef) { 
    this.setHeight(this.defaultHeight);
    this.setBorder(this.initialColor);
  }
  
  private initialColor = '#F5F5F5';
  private defaultColor = '#009688';
  private defaultHeight =  180;

  @Input() borderColor: string;

  @HostListener('mouseenter') onMouseEnter(){
    this.setBorder(this.borderColor || this.defaultColor);
  }

  @HostListener('mouseleave') onMouseLeave(){
    this.setBorder(this.initialColor);
  }


  setHeight(height : number){
    this.el.nativeElement.style.height = `${height}px`;
  }

  setBorder(color : string){
    this.el.nativeElement.style.border = `4px solid ${color}`
  }

}
