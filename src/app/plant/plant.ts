export class Plant {
    id: number;
    exposure: string;
    watering: string;
    name: string;
    picture: string;
    types: Array<string>;
    created: Date;
  }