import { Component } from '@angular/core';
  
@Component({
    selector: 'page-404',
    template: `
    <div class='center'>
      <img style="width: 25%; margin-top: 1rem" src="https://static.vecteezy.com/ti/vecteur-libre/p1/3453404-error-404-with-the-mignon-pot-de-plante-mascotte-gratuit-vectoriel.jpg"/>
      <h1 style= "margin: -2rem 0 1.5rem 0 !important">Hey, cette page n'existe pas !</h1>
      <a routerLink="/plants" class="waves-effect waves-teal btn-flat">
        Retourner à l' accueil
      </a>
    </div>
  `

})
export class PageNotFoundComponent { }
